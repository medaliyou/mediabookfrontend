
export class Category {
  public id:number;
  public designation: string;
  public description: string;

  constructor(id: number, designation: string, description: string) {
    this.id = id;
    this.designation = designation;
    this.description = description;
  }
}
