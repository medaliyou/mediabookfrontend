import {Book} from "./book";
import {User} from "./user";
export enum State {
  pending="pending",
  terminated="terminated"
}
export class Borrow {

  public id: number;
  public user: User;
  public book: Book;
  public borrowDate: Date ;
  public returnDate: Date ;
  public state: State;


  constructor( user: User, book: Book, borrowDate: Date, returnDate: Date, state: State) {

    this.user = user;
    this.book = book;
    this.borrowDate = borrowDate;
    this.returnDate = returnDate;
    this.state = state;
  }
}
