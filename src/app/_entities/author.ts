
export class Author {
  public id: number;
  public firstName: string;
  public lastName: string;
  public biography: string;


  constructor(id: number, firstName: string, lastName: string, biography: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.biography = biography;
  }
}
