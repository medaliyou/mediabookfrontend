import {Author} from "./author";
import {Editor} from "./editor";
import {Category} from "./category";

export class Book {
  public id: number;
  public title: string;
  public pages: number;
  public editionDate: string;
  public stockNumber: number;
  public price: number;
  public picture: string;
  public isbn: string;
  public authors: Author[];
  public editors: Editor[];
  public category: Category;

  constructor(id: number, title: string, pages: number, editionDate: string, stockNumber: number, price: number, picture: string, isbn: string, authors: Author[], editors: Editor[], category: Category) {
    this.id = id;
    this.title = title;
    this.pages = pages;
    this.editionDate = editionDate;
    this.stockNumber = stockNumber;
    this.price = price;
    this.picture = picture;
    this.isbn = isbn;
    this.authors = authors;
    this.editors = editors;
    this.category = category;
  }
}
