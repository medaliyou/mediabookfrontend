
export class Editor {
  public id: number
  public firstName: string;
  public lastName: string;
  public country: string;
  public address: string;
  public phone: number;


  constructor(id: number, firstName: string, lastName: string, country: string, address: string, phone: number) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.country = country;
    this.address = address;
    this.phone = phone;
  }
}
