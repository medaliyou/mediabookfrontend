import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ITokenData, TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {FormControl, FormGroup, NgForm} from "@angular/forms";
import {RegisterService} from "../../services/register.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  ngOnInit(): void {
  }
  constructor(private auth: TokenAuthService,private registerService: RegisterService, private router: Router) {
    if (auth.connected) {
      this.router.navigate([this.auth.config.path.login]);
    }
  }
  public register() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
      this.registerService.register(
        this.form.value.username,
        this.form.value.email,
        this.form.value.password)
        .subscribe((data) => {
          console.log(data);
          this.router.navigate(['/']);
      }, (error) => {
          console.log(error);
      });
    }
  }

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

}
