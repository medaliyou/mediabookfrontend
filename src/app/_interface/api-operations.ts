import { Observable } from 'rxjs';

export interface ApiOperations<T, ID> {
  getAll(): Observable<T[]>;
  getById(id: ID): Observable<T>;
  edit(id: ID, t: T): Observable<T>;
  delete(id: ID): Observable<T>;
  add(t: T): Observable<T>;

}
