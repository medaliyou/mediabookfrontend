import { Component, OnInit } from '@angular/core';
import {ITokenData, TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {Book} from "../../_entities/book";
import {ApiBookService} from "../../services/api-book.service";
import {ApiUploadService} from "../../services/api-upload.service";

import {ConfirmationDialogService} from "../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  books: Book[];
  medias: any;
  authors: any;
  category: any;
  gridColumns = 3;

  constructor(private auth: TokenAuthService,
              private router: Router,
              private apiBookService: ApiBookService,
              private apiUploadService: ApiUploadService,
              private confirmationDialogService: ConfirmationDialogService,
   ) {
    this.medias = [];
    /*
    if (!auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
    */
  }

  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }
  getAuthorsForEachBook(){
    let m = new Map();
    for(let key in this.books) {
      let authors = this.books[key].authors;
      authors.forEach(author => {
        m.set(this.books[key].id, `${author.firstName} ${author.lastName}`);
      });
    }
    return m;

  }
  getCategoryForEachBook(){
    let m = new Map();
    for(let key in this.books){
      let category = this.books[key].category ?  this.books[key].category.designation : null ;
      m.set(this.books[key].id, category);
    }
    return m;
   }

  getMediaForEachBook(){
    let m = new Map();

    for(let key in this.books){
      let ss = this.books[key].picture.split("/");
      let id = parseInt(ss[ss.length - 1]);
      this.apiUploadService.getById(id).subscribe(media => {
        m.set(this.books[key].id, media.contentUrl);
      });
    }
    return m;

  }
  ngOnInit(): void {
    this.apiBookService.getAll().subscribe( (books) => {
        console.log(books);
        this.books = books;
        this.medias = this.getMediaForEachBook();
        this.authors = this.getAuthorsForEachBook();
        this.category = this.getCategoryForEachBook();

      console.log(this.medias);
      }, (error) => {
        console.log(error);
      }
    );

  }


  openBorrowDialog(bookId: number) {
   this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to Borrow this Book ?')
  .then(
  (confirmed) => {
    console.log('User confirmed:', confirmed);
    if(confirmed){
      window.localStorage.removeItem("borrowBookId");
      window.localStorage.setItem("borrowBookId", bookId.toString());
      this.router.navigate(['/borrow']);
    }
  }

  )
  .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));

    }
}
