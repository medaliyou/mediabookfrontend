import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiBookService} from "../../services/api-book.service";
import {ConfirmationDialogService} from "../../confirmation-dialog/confirmation-dialog.service";
import {ApiUserService} from "../../services/api-user.service";
import {User} from "../../_entities/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Borrow, State} from "../../_entities/borrow";
import {ApiBorrowService} from "../../services/api-borrow.service";
import {Book} from "../../_entities/book";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.css']
})
export class BorrowComponent implements OnInit {

  constructor(
    private auth: TokenAuthService,
    private router: Router,
    private apiBookService: ApiBookService,
    private apiUserService: ApiUserService,
    private apiBorrowService: ApiBorrowService,
    private confirmationDialogService: ConfirmationDialogService,
    private formBuilder: FormBuilder,
    private cdRef: ChangeDetectorRef,
    private datePipe: DatePipe
  ) {
    if (!auth.connected) {
      this.router.navigate(['/login']);
      return;
    }

  }

  user: User;
  book: Book;
  addForm: FormGroup;
  borrow: Borrow;
  currentDate;

  async ngOnInit() {
    this.currentDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
    await this.apiUserService.getMeAction({username: this.auth.username}).subscribe(
      (user) => {

        if (typeof user === "string") {
          this.user = JSON.parse(user);
          console.log(this.user)
        }
      }, (error => {
        console.log(error);
      })
    );
    let bookId = localStorage.getItem('borrowBookId');
    await this.apiBookService.getById(+bookId).subscribe(
      (book) => {
        this.book = book;
        console.log(this.book)

      }, (error => {
        console.log(error);
      })
    );

    this.addForm = this.formBuilder.group({
      borrowDate: ['', Validators.required],
      returnDate: ['', Validators.required],
    });


  }

  calculateDiff(borrowDate: Date, returnDate: Date): number {
    return Math.floor(( returnDate.getTime() - borrowDate.getTime() ) / (1000 * 60 * 60 * 24) );
  }

  async onSubmit() {
      let borrow = {
        user: `/api/users/${this.user.id}`,
        book: `/api/books/${this.book.id}`,
        borrowDate: this.currentDate,
        returnDate: this.addForm.get('returnDate').value,
        state: State.pending
        };
      let days = this.calculateDiff(new Date(this.currentDate), new Date(borrow.returnDate));
      let cost = days * this.book.price;
      this.confirmationDialogService.confirm('Confirm Purchase', `Borrowing this book for ${days.toString()} days will cost you $ ${cost.toString()} ?`)
        .then( (confirmed) => {
            console.log('User confirmed:', confirmed);
            if (confirmed == true) {
              console.log(borrow);
              this.apiBorrowService.add(borrow).subscribe(data => {
                this.router.navigate(['/member']);
              });

            }
          })
        .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));

  }





  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  cancelBorrow() {
    localStorage.removeItem('borrowBookId');
    this.router.navigate(['/index']);
  }

}
