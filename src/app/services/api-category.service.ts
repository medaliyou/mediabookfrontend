import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Editor} from "../_entities/editor";
import {Category} from "../_entities/category";
import {ApiService} from "./api-service";

const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json',   })
};

@Injectable({
  providedIn: 'root'
})
export class ApiCategoryService extends ApiService<Category, number>{

  constructor(protected http: HttpClient) {
    super(http, 'categories');
  }
}
