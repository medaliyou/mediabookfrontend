import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  @Injectable({
    providedIn: 'root'
  })
  export class RegisterService {

  constructor(private http: HttpClient) { }

  register(username, email, password): Observable<any> {
    return this.http.post(`${environment.apiUri}/register`,
      {
        username,
        email,
        password
      }, httpOptions
    );
  }

}
