import { TestBed } from '@angular/core/testing';

import { ApiEditorService } from './api-editor.service';

describe('ApiEditorService', () => {
  let service: ApiEditorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiEditorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
