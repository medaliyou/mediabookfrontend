import { Injectable } from '@angular/core';
import {ApiService} from "./api-service";
import {Editor} from "../_entities/editor";
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data'  }),
  reportProgress: true,
 };

@Injectable({
  providedIn: 'root'
})
export class ApiUploadService extends ApiService<any, number>{
  constructor(protected http: HttpClient) {
    super(http, 'media_objects');

  }
  addImage(t: any){
    const base = `${environment.apiUri}/api`
    return this.http.post(`${base}/${this._base}`, t, httpOptions);
  }
  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);
    const base = `${environment.apiUri}/api`

    const request = new HttpRequest('POST', `${base}/${this._base}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(request);
  }


}

