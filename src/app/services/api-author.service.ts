import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

import {Author} from "../_entities/author";
import {ApiService} from "./api-service";


const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json',   })
};

@Injectable({
  providedIn: 'root'
})
export class ApiAuthorService extends ApiService<Author, number>{

  constructor(protected http: HttpClient) {
    super(http, 'authors');
  }

}
