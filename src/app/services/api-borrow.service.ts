import { Injectable } from '@angular/core';
import {ApiService} from "./api-service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json',   })
};
@Injectable({
  providedIn: 'root'
})
export class ApiBorrowService extends ApiService<Object, number>{

  constructor(protected http: HttpClient) {
    super(http, 'borrows');
  }
  getMyBorrows(data){
    return this.http.post(`${environment.apiUri}/api/borrows/get_me`, data, httpOptions);
  }
}
