import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {User} from "../_entities/user";
import {ApiService} from "./api-service";
const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json',   })
};

@Injectable({
  providedIn: 'root'
})
export class ApiUserService extends ApiService<User, number>{



  constructor(protected http: HttpClient) {
    super(http, 'users');
  }
  getMeAction(username){
    return this.http.post(`${environment.apiUri}/api/users/get_me`, username, httpOptions);
  }
}
