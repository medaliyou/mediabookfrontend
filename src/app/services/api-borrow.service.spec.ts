import { TestBed } from '@angular/core/testing';

import { ApiBorrowService } from './api-borrow.service';

describe('ApiBorrowService', () => {
  let service: ApiBorrowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiBorrowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
