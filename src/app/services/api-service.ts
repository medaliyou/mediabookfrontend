import {ApiOperations} from "../_interface/api-operations";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json','Accept': 'application/json'   })
};

const base = `${environment.apiUri}/api`

export abstract class ApiService<T, ID> implements ApiOperations<T, ID>{

  protected constructor(
    protected http: HttpClient,
    protected _base: string,
   ) { }



  add(t: T): Observable<T> {
    return this.http.post<T>(`${base}/${this._base}`, t, httpOptions);
  }

  delete(id: ID): Observable<T> {
    return this.http.delete<T>(`${base}/${this._base}/${id}`,  httpOptions);
  }

  edit(id: ID, t: T): Observable<T> {
    return this.http.put<T>(`${base}/${this._base}/${id}`, t, httpOptions);
  }

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(`${base}/${this._base}`, httpOptions);
  }

  getById(id: ID): Observable<T> {
    return this.http.get<T>(`${base}/${this._base}/${id}`, httpOptions);
  }

}
