import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../_entities/user";
import {Book} from "../_entities/book";
import {ApiService} from "./api-service";

const httpOptions = {
  headers: new HttpHeaders({  'Content-Type': 'application/json',   })
};

@Injectable({
  providedIn: 'root'
})
export class ApiBookService extends ApiService<Book, number>{

  constructor(protected http: HttpClient) {
    super(http, 'books');
  }


}
