import { Component, OnInit } from '@angular/core';
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiUserService} from "../../services/api-user.service";
import {error} from "@angular/compiler/src/util";
import {Book} from "../../_entities/book";
import {User} from "../../_entities/user";
import {Borrow} from "../../_entities/borrow";
import {ApiBorrowService} from "../../services/api-borrow.service";
import {ApiBookService} from "../../services/api-book.service";

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  user: User;
  borrows: Borrow[];
  displayedColumns=[
    "id",
    "book" ,
    "borrowDate",

    "returnDate",
    "state"

  ];
  bookTitles = new Map();
  constructor(private auth: TokenAuthService, private apiUserService: ApiUserService, private apiBorrowService: ApiBorrowService, private apiBookService: ApiBookService) {
    //let username = this.auth.;
    this.ngOnInit();
  }

  async ngOnInit() {
    const t = await this.apiUserService.getMeAction({username: this.auth.username}).toPromise();
    if (typeof t === "string") {
      const u = JSON.parse(t);
      this.user = u ;
    }

    const f = await this.apiBorrowService.getMyBorrows({user: this.user.id}).toPromise();
    if (typeof f === "string") {
      const u = JSON.parse(f);
      this.borrows = u ;
    }

    this.borrows.forEach(borrow => {
      const id = (borrow.book).toString().split("/");
      let rid = id[id.length - 1];
      this.apiBookService.getById(+rid).subscribe((book) => {
        this.bookTitles.set(borrow.id, book.title);
      }, (error) => {
        console.log(error);
      });
     })

  }

  public logout() {
    this.auth.logout();
  }

  getBookTitle(bookId: string) {
    let title;
    console.log(bookId);
    const id = bookId.split("/");
    let rid = id[id.length - 1];
    this.apiBookService.getById(+rid).subscribe((book) => {
      title = book.title;
    }, (error) => {
      console.log(error);
    });
    return title;

  }
}
