import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TokenAuthService} from "ng-token-auth-symfony";
import {NavigationEnd, Router} from "@angular/router";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Media Books';
  private returnUrl = '/';

  admin: any;
  connected;
  @Output() toggleSidenav = new EventEmitter<void>();


  constructor(public auth: TokenAuthService, private router: Router) {
    if (auth.tokenNotExpired() && auth.connected) {
      this.connected = auth.connected;
      this.admin = this.auth.roles.includes('ROLE_ADMIN');
      console.log(this.admin);
      this.router.navigate([this.auth.config.path.login] );
    }
    else if(! auth.tokenNotExpired()){
      this.auth.refresh();
      this.router.events.subscribe((event) => {

        if (event instanceof NavigationEnd) {

          this.returnUrl = event.url;
        }

      });
      this.router.navigate([this.returnUrl]);
    }

  }

  public onProfile() {

    this.router.navigate(['users/profile']);
  }

  public logout() {

    this.auth.logout( );
  }

  ngOnInit(): void {

  }
}
