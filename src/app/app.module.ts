import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ITokenConfig, TokenAuthModule} from "ng-token-auth-symfony";
import { environment } from "../environments/environment";
import { AppRoutingModule } from './app-routing.module';
import { IndexComponent } from './_index/index/index.component';
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MemberComponent } from './_member/member/member.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './_register/register/register.component';
import { LoginComponent } from './_login/login/login.component';
import {MatCardModule} from "@angular/material/card";
import { ListUserComponent } from './admin/user/list-user/list-user.component';
import { AddUserComponent } from './admin/user/add-user/add-user.component';
import { EditUserComponent } from './admin/user/edit-user/edit-user.component';
import { ListBookComponent } from './admin/book/list-book/list-book.component';
import { AddBookComponent } from './admin/book/add-book/add-book.component';
import { EditBookComponent } from './admin/book/edit-book/edit-book.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {DatePipe} from "@angular/common";
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenavModule} from "@angular/material/sidenav";
import {ExtendedModule, FlexModule} from "@angular/flex-layout";
import {MatGridListModule} from "@angular/material/grid-list";
import { LayoutModule } from '@angular/cdk/layout';
import { MatListModule } from '@angular/material/list';
import {_MatMenuDirectivesModule, MatMenuModule} from "@angular/material/menu";
import {MatInputModule} from "@angular/material/input";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import { AddAuthorComponent } from './admin/author/add-author/add-author.component';
import { ListAuthorComponent } from './admin/author/list-author/list-author.component';
import { EditAuthorComponent } from './admin/author/edit-author/edit-author.component';
import { EditEditorComponent } from './admin/editor/edit-editor/edit-editor.component';
import { AddEditorComponent } from './admin/editor/add-editor/add-editor.component';
import { ListEditorComponent } from './admin/editor/list-editor/list-editor.component';
import { ListCategoryComponent } from './admin/category/list-category/list-category.component';
import { AddCategoryComponent } from './admin/category/add-category/add-category.component';
import { EditCategoryComponent } from './admin/category/edit-category/edit-category.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { BorrowComponent } from './_borrow/borrow/borrow.component';
import {MatChipsModule} from "@angular/material/chips";


const tokenConfig: ITokenConfig = {
  url: {
    login: `${environment.apiUri}/api/login_check`,
    refresh: `${environment.apiUri}/api/login_check`
  },
  path: {
    login: '/',
    logout: '/login',
  }
};

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    MemberComponent,
    RegisterComponent,
    LoginComponent,
    ListUserComponent,
    AddUserComponent,
    EditUserComponent,
    ListBookComponent,
    AddBookComponent,
    EditBookComponent,
    AddAuthorComponent,
    ListAuthorComponent,
    EditAuthorComponent,
    EditEditorComponent,
    AddEditorComponent,
    ListEditorComponent,
    ListCategoryComponent,
    AddCategoryComponent,
    EditCategoryComponent,
    ConfirmationDialogComponent,
    BorrowComponent,
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    TokenAuthModule.forRoot(tokenConfig),
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    ExtendedModule,
    MatSidenavModule,
    FlexModule,
    ExtendedModule,
    MatGridListModule,
    LayoutModule,
    MatListModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    MatInputModule,
    MatButtonToggleModule,
    MatChipsModule,


  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
