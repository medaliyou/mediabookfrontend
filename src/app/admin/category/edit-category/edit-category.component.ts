import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Author} from "../../../_entities/author";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiAuthorService} from "../../../services/api-author.service";
import {first} from "rxjs/operators";
import {ApiCategoryService} from "../../../services/api-category.service";
import {Category} from "../../../_entities/category";

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {

  category: Category;
  editForm: FormGroup;



  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private auth: TokenAuthService,
              private apiCategoryService: ApiCategoryService,
              private cdRef: ChangeDetectorRef,
  ) {
    if (!auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnInit() {

    let categoryId = window.localStorage.getItem("editCategoryId");
    console.log(categoryId);
    if(!categoryId) {
      alert("Invalid action.")
      this.router.navigate(['admin/list-category']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      designation: ['', Validators.required],
      description: ['', Validators.required],


    });

    this.apiCategoryService.getById(+categoryId)
      .subscribe( data => {
        let category = new Category(
          data['id'],
          data['designation'],
          data['description']
        );
        this.editForm.setValue(category);
      });

  }
  onSubmit() {


    let category : Category = this.editForm.value;


    console.log(category);

    this.apiCategoryService.edit(category.id, category)
      .pipe(first())
      .subscribe(
        data => {
          alert('Category updated successfully.');
          this.router.navigate(['admin/list-category']);

        },
        error => {
          alert(error);
        });
  }


}
