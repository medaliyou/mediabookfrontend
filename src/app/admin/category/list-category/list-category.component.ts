import { Component, OnInit } from '@angular/core';
import {Author} from "../../../_entities/author";
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiAuthorService} from "../../../services/api-author.service";
import {ApiCategoryService} from "../../../services/api-category.service";
import {Category} from "../../../_entities/category";
import {Book} from "../../../_entities/book";
import {ConfirmationDialogService} from "../../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {


  categories: Category[];
   displayedColumns = [
    "Id",
    "Designation",
    "Description",
   "Actions",

  ];

  constructor(private auth: TokenAuthService, private router: Router, private apiCategoryService: ApiCategoryService, private confirmationDialogService: ConfirmationDialogService) {
    this.apiCategoryService = apiCategoryService;
    this.router = router;
    this.auth = auth;
    if (!this.auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
  }

  async ngOnInit() {
    await this.apiCategoryService.getAll().subscribe((categories) => {
      this.categories = categories;

    }, (error) => {
      console.log(error);
    });
  }

  addCategory() {
    this.router.navigate(['admin/add-category']);
  }

  deleteCategory(category: Category) {
    this.apiCategoryService.delete(category.id).subscribe(data => {
      this.categories = this.categories.filter(u => u !== category);
    });
  }

  editCategory(category: Category) {
    window.localStorage.removeItem("editCategoryId");
    window.localStorage.setItem("editCategoryId", category.id.toString());
    this.router.navigate(['admin/edit-category']);
  }
  public openConfirmationDialog(category: Category) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete Category ?')
      .then(
        (confirmed) => {
          console.log('User confirmed:', confirmed);
          if(confirmed){
            this.deleteCategory(category);
          }
        }

      )
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
