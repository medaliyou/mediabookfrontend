import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Author} from "../../../_entities/author";
import {Router} from "@angular/router";
import {ApiAuthorService} from "../../../services/api-author.service";
import {TokenAuthService} from "ng-token-auth-symfony";
import {Category} from "../../../_entities/category";
import {ApiCategoryService} from "../../../services/api-category.service";

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  addForm: FormGroup;
  category: Category;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private apiCategoryService: ApiCategoryService,
              private auth: TokenAuthService,
              private cdRef: ChangeDetectorRef
  ) {
    if (! this.auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }

  ngOnInit(): void {

    this.addForm = this.formBuilder.group({
      designation: ['', Validators.required],
      description: ['', Validators.required],

    });

  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


  onSubmit() {

    this.category = this.addForm.value;


    this.apiCategoryService.add(this.category)
      .subscribe(data => {
        this.router.navigate(['/admin/list-category']);
      });
  }



}

