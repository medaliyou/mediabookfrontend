import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Book} from "../../../_entities/book";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiBookService} from "../../../services/api-book.service";
import {ApiAuthorService} from "../../../services/api-author.service";
import {ApiEditorService} from "../../../services/api-editor.service";
import {ApiCategoryService} from "../../../services/api-category.service";
import {ApiUploadService} from "../../../services/api-upload.service";
import {DatePipe} from "@angular/common";
import {first} from "rxjs/operators";
import {Author} from "../../../_entities/author";

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.css']
})
export class EditAuthorComponent implements OnInit {

  author: Author;
  editForm: FormGroup;



  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private auth: TokenAuthService,
              private apiAuthorsService: ApiAuthorService,
              private cdRef: ChangeDetectorRef,
  ) {
    if (!auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnInit() {

    let authorId = window.localStorage.getItem("editAuthorId");
    console.log(authorId);
    if(!authorId) {
      alert("Invalid action.")
      this.router.navigate(['admin/list-author']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      biography: ['', Validators.required],


    });

    this.apiAuthorsService.getById(+authorId)
      .subscribe( data => {
        let author = new Author(
          data['id'],
          data['firstName'],
          data['lastName'],
          data['biography']
        );
        this.editForm.setValue(author);
      });

  }
  onSubmit() {


    let author : Author = this.editForm.value;


    console.log(author);

    this.apiAuthorsService.edit(author.id, author)
      .pipe(first())
      .subscribe(
        data => {
          alert('Author updated successfully.');
          this.router.navigate(['admin/list-author']);

        },
        error => {
          alert(error);
        });
  }

}


