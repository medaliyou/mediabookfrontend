import { Component, OnInit } from '@angular/core';
import {Author} from "../../../_entities/author";
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiAuthorService} from "../../../services/api-author.service";
import {Book} from "../../../_entities/book";
import {ConfirmationDialogService} from "../../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-list-author',
  templateUrl: './list-author.component.html',
  styleUrls: ['./list-author.component.css']
})
export class ListAuthorComponent implements OnInit {

  authors: Author[];
  displayedColumns = [
    "Id",
    "First Name",
    "Last Name",
    "Biography",
    "Actions",

  ];

  constructor(private auth: TokenAuthService, private router: Router, private apiAuthorService: ApiAuthorService, private confirmationDialogService: ConfirmationDialogService) {
    this.apiAuthorService = apiAuthorService;
    this.router = router;
    this.auth = auth;
    if (!this.auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
  }

  async ngOnInit() {
    await this.apiAuthorService.getAll().subscribe((authors) => {
      this.authors = authors;

    }, (error) => {
      console.log(error);
    });
  }

  addAuthor() {
    this.router.navigate(['admin/add-author']);
  }

  deleteAuthor(author: Author) {
    this.apiAuthorService.delete(author.id).subscribe(data => {
      this.authors = this.authors.filter(u => u !== author);
    });
  }

  editAuthor(author: Author) {
    window.localStorage.removeItem("editAuthorId");
    window.localStorage.setItem("editAuthorId", author.id.toString());
    this.router.navigate(['admin/edit-author']);
  }

  public openConfirmationDialog(author: Author) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete Author ?')
      .then(
        (confirmed) => {
          console.log('User confirmed:', confirmed);
          if(confirmed){
            this.deleteAuthor(author);
          }
        }

      )
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
