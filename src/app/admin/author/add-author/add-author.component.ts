import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";

import {ApiAuthorService} from "../../../services/api-author.service";

import {Author} from "../../../_entities/author";
@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent implements OnInit {
  addForm: FormGroup;
  author: Author;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private apiAuthorsService: ApiAuthorService,
              private auth: TokenAuthService,
              private cdRef: ChangeDetectorRef
  ) {
    if (! this.auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }

  ngOnInit(): void {

    this.addForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      biography: ['', Validators.required],


    });

  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


  onSubmit() {

    this.author = this.addForm.value;


    this.apiAuthorsService.add(this.author)
      .subscribe(data => {
        this.router.navigate(['/admin/list-author']);
      });
  }



}
