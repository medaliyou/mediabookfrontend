import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiBookService} from "../../../services/api-book.service";
import {Book} from "../../../_entities/book";
import {ApiAuthorService} from "../../../services/api-author.service";
import {ApiEditorService} from "../../../services/api-editor.service";
import {ApiCategoryService} from "../../../services/api-category.service";
import {Author} from "../../../_entities/author";
import {Category} from "../../../_entities/category";
import {ApiUploadService} from "../../../services/api-upload.service";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse, HttpEventType, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  authorsFormControl = new FormControl();
  editorsFormControl = new FormControl();
  categoryFormControl = new FormControl();

  addForm: FormGroup;

  book: Book;

  authorsCollection ;
  categoriesCollection;
  editorsCollection;
  selectedFiles: FileList;
  progressInfos = [];
  message = '';
  fileInfos: Observable<any>;
  bookPicture: string;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private apiBookService: ApiBookService,
              private apiAuthorsService: ApiAuthorService,
              private apiEditorService: ApiEditorService,
              private apiCategoryService: ApiCategoryService,
              private apiUploadService: ApiUploadService,
              private auth: TokenAuthService,
              private cdRef: ChangeDetectorRef
  ) {
    if (! this.auth.connected) {
      this.router.navigate(['/']);
      return;
    }
    this.authorsCollection = [];
    this.categoriesCollection = [];
    this.editorsCollection = [];


  }


  ngOnInit(): void {
    this.apiAuthorsService.getAll().subscribe((authors)=>{
      const b = authors;
      console.log(authors);

      for(let i=0;i<b.length; i++){
        let c =  { id: b[i]['id'], firstName: b[i].firstName, lastName: b[i].lastName }
        this.authorsCollection.push(c);
      }
    }, (error => {
      console.log(error)
    }));

    this.apiCategoryService.getAll().subscribe((categories)=>{

        const b = categories;
      console.log(categories);

      for(let i=0;i<b.length; i++){
          let c =  { id: b[i]['id'], designation: b[i].designation  }
          this.categoriesCollection.push(c);
        }
      }, (error) => {
        console.log(error)
      });

    this.apiEditorService.getAll().subscribe((editors)=>{

      const b = editors;
      console.log(editors);
      for(let i=0;i<b.length; i++){
        let c =  { id: b[i]['id'], firstName: b[i].firstName, lastName: b[i].lastName }
        this.editorsCollection.push(c);
      }
    }, (error) => {
      console.log(error)
    });


    console.log(this.authorsCollection)


    this.addForm = this.formBuilder.group({
      title: ['', Validators.required],
      pages: ['', Validators.required],
      editionDate: ['', Validators.required],
      stockNumber: ['', Validators.required],
      isbn: ['', Validators.required],
      price: ['', Validators.required],
      picture: ['', Validators.required],
      authors: ['', Validators.required],
      editors: ['', Validators.required],
      category: ['', Validators.required],

    });

  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


  onSubmit() {
    let fAuthors = [];
    let fEditors = []
    let fCategory ;

    this.book = this.addForm.value;
    if(this.authorsFormControl.value){
      for(let i of this.authorsFormControl.value){
        fAuthors.push(`/api/authors/${i.id}`);
      }
    }
    if(this.editorsFormControl.value ) {

      for (let j of this.editorsFormControl.value) {
        fEditors.push(`/api/editors/${j.id}`);
      }
    }
    if(this.categoryFormControl.value){
      fCategory = `/api/categories/${this.categoryFormControl.value.id}`;

    }
    this.book.authors = fAuthors;
    this.book.editors = fEditors;
    this.book.category = fCategory;
    this.book.picture = this.bookPicture;
    console.log('this.book.picture')
    console.log(this.book.picture);

    this.apiBookService.add(this.book)
      .subscribe(data => {
        this.router.navigate(['/admin/list-book']);
      });
  }

  selectFiles(e): void {
    this.progressInfos = [];
    this.selectedFiles = e.target.files;
  }

  uploadFiles(): void {
    this.message = '';

    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

  async upload(idx, file) {
    this.progressInfos[idx] = { value: 0, fileName: file.name };

    await this.apiUploadService.upload(file).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
           this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.bookPicture = event.body['@id'];
          console.log('this.bookPicture');
          console.log(this.bookPicture);
        }
      },
      error => {
        this.progressInfos[idx].value = 0;
        this.message = 'Could not upload the file:' + file.name;
        console.error('error: ', error)
      });
  }


  categoryEvent($event: Event) {
    console.log(this.categoryFormControl.value);
    console.log($event);
  }
}
