import { Component, OnInit } from '@angular/core';
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiUserService} from "../../../services/api-user.service";
import {Book} from "../../../_entities/book";
import {ApiBookService} from "../../../services/api-book.service";
import {User} from "../../../_entities/user";
import {ApiUploadService} from "../../../services/api-upload.service";
import {ConfirmationDialogService} from "../../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.css']
})
export class ListBookComponent implements OnInit {

  books: Book[];
  displayedColumns=[
    "Id",
    "Title" ,
    "Pages",
    "Edition Date",
    "Stock Quantity",
    "Price",
    "ISBN",
    "Authors",
    "Editors",
    "Category",
    "Picture",
    "Actions"
  ];
  public category: Map<any, any>;
  public authors: Map<any, any>;
  public editors: Map<any, any>;
  public media: Map<any, any>;
  constructor(private auth: TokenAuthService,
              private router: Router,
              private apiBookService:  ApiBookService,
              private apiUploadService: ApiUploadService,
              private confirmationDialogService: ConfirmationDialogService,
  ) {
    this.apiBookService = apiBookService;
    this.router = router;
    this.auth = auth;
    if (!this.auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
  }
  async ngOnInit() {
    await this.apiBookService.getAll().subscribe((books) => {
      this.books = books;
      console.log(this.books)
      this.category = this.getCategoryForEachBook();
      console.log(this.category)
      this.authors = this.getAuthorsForEachBook();
      console.log(this.authors)
      this.editors = this.getEditorsForEachBook();
      console.log(this.editors );

      this.media = this.getMediaForEachBook();
      console.log(this.media );

      }, (error) => {
      console.log(error);
    });
  }
  addBook() {
    this.router.navigate(['admin/add-book']);
  }

  deleteBook(book: Book) {
    this.apiBookService.delete(book.id).subscribe( data => {
      this.books = this.books.filter(u => u !== book);
    });
  }
  editBook(book: Book ) {
    window.localStorage.removeItem("editBookId");
    window.localStorage.setItem("editBookId", book.id.toString());
    this.router.navigate(['admin/edit-book']);
  }

  getAuthorsForEachBook(){
    let m = new Map();
    for(let key in this.books) {
      let authors = this.books[key].authors;
      authors.forEach(author => {
        m.set(this.books[key].id, `${author.firstName} ${author.lastName}`);
      });
    }
    return m;

  }
  getCategoryForEachBook(){
    let m = new Map();
    for(let key in this.books){
      let category = this.books[key].category ? this.books[key].category.designation : null;
      m.set(this.books[key].id, `${category} `);
    }
    return m;
  }

  getMediaForEachBook(){
    let m = new Map();

    for(let key in this.books){
      let ss = this.books[key].picture.split("/");
      let id = parseInt(ss[ss.length - 1]);
      this.apiUploadService.getById(id).subscribe(media => {
        m.set(this.books[key].id, media.contentUrl);
      });
    }
    return m;

  }


  private getEditorsForEachBook() {
    let m = new Map();
    for(let key in this.books) {
      let authors = this.books[key].editors;
      authors.forEach(editor => {
        m.set(this.books[key].id, `${editor.firstName} ${editor.lastName}`);
      });
    }
    return m;

  }

  public openConfirmationDialog(book: Book) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete Book ?')
      .then(
        (confirmed) => {
          console.log('User confirmed:', confirmed);
          if(confirmed){
            this.deleteBook(book);
          }
        }

      )
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

}
