import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";
import {Book} from "../../../_entities/book";
import {ApiBookService} from "../../../services/api-book.service";
import {ApiAuthorService} from "../../../services/api-author.service";
import {ApiEditorService} from "../../../services/api-editor.service";
import {ApiCategoryService} from "../../../services/api-category.service";
import {first} from "rxjs/operators";
import {DatePipe} from "@angular/common";
import {ApiUploadService} from "../../../services/api-upload.service";

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  book: Book;
  editForm: FormGroup;
  authorsFormControl = new FormControl();
  editorsFormControl = new FormControl();
  categoryFormControl = new FormControl();
  authorsCollection ;
  categoriesCollection;
  editorsCollection;


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private auth: TokenAuthService,
              private apiBookService: ApiBookService,
              private apiAuthorsService: ApiAuthorService,
              private apiEditorService: ApiEditorService,
              private apiCategoryService: ApiCategoryService,
              private apiUploadService: ApiUploadService,
              private cdRef: ChangeDetectorRef,
              private datePipe: DatePipe,
  ) {
    if (!auth.connected) {
      this.router.navigate(['/']);
      return;
    }
    this.authorsCollection = [];
    this.categoriesCollection = [];
    this.editorsCollection = [];
    this.datePipe = datePipe;
  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.apiAuthorsService.getAll().subscribe((authors) =>{
      const b = authors ;
      for(let i=0;i<b.length; i++){
        let c =  { id: b[i]['id'], firstName: b[i].firstName, lastName: b[i].lastName }
        this.authorsCollection.push(c);
      }
    }, (error => {
      console.log(error)
    }));

    this.apiCategoryService.getAll().subscribe((categories)=>{

      const b = categories ;
      for(let i=0;i<b.length; i++){
        let c =  { id: b[i]['id'], designation: b[i].designation  }
        this.categoriesCollection.push(c);
      }
    }, (error) => {
      console.log(error)
    });

    this.apiEditorService.getAll().subscribe((editors)=>{

      const b = editors ;
      for(let i=0;i<b.length; i++){
        let c =  { id: b[i]['id'], firstName: b[i].firstName, lastName: b[i].lastName }
        this.editorsCollection.push(c);
      }
    }, (error) => {
      console.log(error)
    });


    let bookId = window.localStorage.getItem("editBookId");
    console.log(bookId);
    if(!bookId) {
      alert("Invalid action.")
      this.router.navigate(['admin/list-book']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      title: ['', Validators.required],
      pages: ['', Validators.required],
      editionDate: ['', Validators.required],
      stockNumber: ['', Validators.required],
      isbn: ['', Validators.required],
      price: ['', Validators.required],
      authors: ['', Validators.required],
      editors: ['', Validators.required],
      category: ['', Validators.required],
      picture: ['', Validators.required],

    });

    this.apiBookService.getById(+bookId)
      .subscribe( data => {
        let book = new Book(
          data['id'],
          data['title'],
          data['pages'],
          this.datePipe.transform(data['editionDate'], 'yyyy-MM-dd'),
          data['stockNumber'],
          data['price'],
          data['picture'],
          data['isbn'],
          data['authors'],
          data['editors'],
          data['category'],
          );
        this.editForm.setValue(book);
      });

  }
  onSubmit() {
    let fAuthors = [];
    let fEditors = []
    let fCategory;
    if(this.authorsFormControl.value){
      for(let i of this.authorsFormControl.value){
        fAuthors.push(`/api/authors/${i.id}`);
      }
    }
    if(this.editorsFormControl.value) {

      for (let j of this.editorsFormControl.value) {
        fEditors.push(`/api/editors/${j.id}`);
      }
    }
    if(this.categoriesCollection.value){
      fCategory = `/api/categories/${this.categoryFormControl.value.id}`;
    }

    let book : Book = this.editForm.value;

    book.authors = fAuthors;
    book.editors = fEditors;
    book.category = fCategory;
    console.log(book);

    this.apiBookService.edit(book.id, book)
      .pipe(first())
      .subscribe(
        data => {
          alert('Book updated successfully.');
          this.router.navigate(['admin/list-book']);

        },
        error => {
          alert(error);
        });
  }

}
