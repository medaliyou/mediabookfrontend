import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Author} from "../../../_entities/author";
import {Router} from "@angular/router";
import {ApiAuthorService} from "../../../services/api-author.service";
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiEditorService} from "../../../services/api-editor.service";
import {Editor} from "../../../_entities/editor";

@Component({
  selector: 'app-add-editor',
  templateUrl: './add-editor.component.html',
  styleUrls: ['./add-editor.component.css']
})
export class AddEditorComponent implements OnInit {

  addForm: FormGroup;
  editor: Editor;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private apiEditorService: ApiEditorService,
              private auth: TokenAuthService,
              private cdRef: ChangeDetectorRef
  ) {
    if (! this.auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }

  ngOnInit(): void {

    this.addForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      country: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
    });

  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


  onSubmit() {

    this.editor = this.addForm.value;


    this.apiEditorService.add(this.editor)
      .subscribe(data => {
        this.router.navigate(['/admin/list-editor']);
      });
  }


}
