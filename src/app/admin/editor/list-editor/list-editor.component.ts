import { Component, OnInit } from '@angular/core';
import {Author} from "../../../_entities/author";
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiAuthorService} from "../../../services/api-author.service";
import {Editor} from "../../../_entities/editor";
import {ApiEditorService} from "../../../services/api-editor.service";
import {Category} from "../../../_entities/category";
import {ConfirmationDialogService} from "../../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-list-editor',
  templateUrl: './list-editor.component.html',
  styleUrls: ['./list-editor.component.css']
})
export class ListEditorComponent implements OnInit {


  editors: Editor[];
  displayedColumns = [
    "Id",
    "First Name",
    "Last Name",
    "Country",
    "Address",
    "Phone",
    "Actions",

  ];

  constructor(private auth: TokenAuthService, private router: Router, private apiEditorService: ApiEditorService, private confirmationDialogService: ConfirmationDialogService) {
    this.apiEditorService = apiEditorService;
    this.router = router;
    this.auth = auth;
    if (!this.auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
  }

  async ngOnInit() {
    await this.apiEditorService.getAll().subscribe((editors) => {
      this.editors = editors;

    }, (error) => {
      console.log(error);
    });
  }

  addEditor() {
    this.router.navigate(['admin/add-editor']);
  }

  deleteEditor(editor: Editor) {
    this.apiEditorService.delete(editor.id).subscribe(data => {
      this.editors = this.editors.filter(u => u !== editor);
    });
  }

  editEditor(author: Author) {
    window.localStorage.removeItem("editEditorId");
    window.localStorage.setItem("editEditorId", author.id.toString());
    this.router.navigate(['admin/edit-editor']);
  }
  public openConfirmationDialog(editor: Editor) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete Editor ?')
      .then(
        (confirmed) => {
          console.log('User confirmed:', confirmed);
          if(confirmed){
            this.deleteEditor(editor);
          }
        }

      )
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
