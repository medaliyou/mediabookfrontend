import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Author} from "../../../_entities/author";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenAuthService} from "ng-token-auth-symfony";
import {ApiAuthorService} from "../../../services/api-author.service";
import {first} from "rxjs/operators";
import {ApiEditorService} from "../../../services/api-editor.service";
import {Editor} from "../../../_entities/editor";

@Component({
  selector: 'app-edit-editor',
  templateUrl: './edit-editor.component.html',
  styleUrls: ['./edit-editor.component.css']
})
export class EditEditorComponent implements OnInit {
  editor: Editor;
  editForm: FormGroup;



  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private auth: TokenAuthService,
              private apiEditorService: ApiEditorService,
              private cdRef: ChangeDetectorRef,
  ) {
    if (!auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnInit() {

    let editorId = window.localStorage.getItem("editEditorId");
    console.log(editorId);
    if(!editorId) {
      alert("Invalid action.")
      this.router.navigate(['admin/list-editor']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      country: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],


    });

    this.apiEditorService.getById(+editorId)
      .subscribe( data => {
        let editor = new Editor(
          data['id'],
          data['firstName'],
          data['lastName'],
          data['country'],
          data['address'],
          data['phone'],

        );
        this.editForm.setValue(editor);
      });

  }
  onSubmit() {


    let editor : Editor = this.editForm.value;


    console.log(editor);

    this.apiEditorService.edit(editor.id, editor)
      .pipe(first())
      .subscribe(
        data => {
          alert('Editor updated successfully.');
          this.router.navigate(['admin/list-editor']);

        },
        error => {
          alert(error);
        });
  }

}
