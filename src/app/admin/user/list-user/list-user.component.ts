import { Component, OnInit } from '@angular/core';
import {TokenAuthService} from "ng-token-auth-symfony";
import {Router} from "@angular/router";
import {ApiUserService} from "../../../services/api-user.service";
import {User} from "../../../_entities/user";
import {Editor} from "../../../_entities/editor";
import {ConfirmationDialogService} from "../../../confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
  users: User[];
  displayedColumns=[
    "Id",
    "username" ,
    "email",
    "roles",
    "Actions",

  ];
  constructor(private auth: TokenAuthService, private router: Router, private apiUserService: ApiUserService, private confirmationDialogService: ConfirmationDialogService) {
    this.apiUserService = apiUserService;
    this.router = router;
    this.auth = auth;
    if (!this.auth.connected) {
      this.router.navigate(['/login']);
      return;
    }
  }

  ngOnInit(): void {
    this.apiUserService.getAll().subscribe((users) => {
      this.users = users ;
      console.log(this.users);
    }, (error) => {
      console.log(error);
    });
  }

  addUser() {
    this.router.navigate(['admin/add-user']);

    //this.apiUserService.addUser(user).subscribe();
  }

  deleteUser(user: any) {
    this.apiUserService.delete(user.id).subscribe( data => {
      this.users = this.users.filter(u => u !== user);
    })

  }

  editUser(user: User ) {
    window.localStorage.removeItem("editUserId");
    window.localStorage.setItem("editUserId", user.id.toString());
    this.router.navigate(['admin/edit-user']);

    //this.apiUserService.editUser(user).subscribe();

  }
  public openConfirmationDialog(user: User) {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete User ?')
      .then(
        (confirmed) => {
          console.log('User confirmed:', confirmed);
          if(confirmed){
            this.deleteUser(user);
          }
        }

      )
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
