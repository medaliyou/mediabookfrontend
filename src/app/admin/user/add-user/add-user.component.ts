import { Component, OnInit , AfterViewInit,  ChangeDetectorRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiUserService} from "../../../services/api-user.service";
import {TokenAuthService} from "ng-token-auth-symfony";
import {User} from "../../../_entities/user";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {


  constructor(private formBuilder: FormBuilder, private router: Router, private apiUserService: ApiUserService, private auth: TokenAuthService, private cdRef: ChangeDetectorRef) {
    if (! this.auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }

  addForm: FormGroup;
  options = [
    {name:'User', value:'ROLE_USER', checked:true},
    {name:'Admin', value:'ROLE_ADMIN', checked:false},
  ]
  user: User;



  ngOnInit() {
    this.addForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      plainPassword: ['', Validators.required],
      roles: ['', Validators.required],
    });

  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


  get selectedOptions() {
    return this.options
      .filter(opt => opt.checked)
      .map(opt => opt.value)
  }

  onSubmit() {
    this.user = this.addForm.value;
    this.user.roles = this.selectedOptions ;
    console.log(this.user);
    this.apiUserService.add(this.user)
      .subscribe( data => {
        console.log(data);
        this.router.navigate(['/admin/list-user']);
      });
  }



}
