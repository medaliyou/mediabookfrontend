import {ChangeDetectorRef, Component, OnInit, AfterViewInit} from '@angular/core';
import {User} from "../../../_entities/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiUserService} from "../../../services/api-user.service";
import {first} from "rxjs/operators";
import {TokenAuthService} from "ng-token-auth-symfony";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  user: User;
  editForm: FormGroup;
  options = [
    {name:'User', value:'ROLE_USER', checked:true},
    {name:'Admin', value:'ROLE_ADMIN', checked:false},
  ]

  constructor(private formBuilder: FormBuilder, private router: Router, private auth: TokenAuthService, private apiUserService: ApiUserService, private cdRef: ChangeDetectorRef) {
    if (! auth.connected) {
      this.router.navigate(['/']);
      return;
    }
  }
  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnInit(): void {
    let userId = window.localStorage.getItem("editUserId");
    console.log(userId);
    if(!userId) {
      alert("Invalid action.")
      this.router.navigate(['admin/list-user']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      username: ['', Validators.required],
      email: ['', Validators.required],
      plainPassword: ['', Validators.required],
      roles: ['', Validators.required],
    });
    this.apiUserService.getById(+userId)
      .subscribe( data => {
        let user: User = new User(data['id'], data['username'], data['email'],data['roles'], data['plainPassword'], );
        console.log(user);
        this.editForm.setValue(user);
      });
  }

  onSubmit() {
    let user : User = this.editForm.value;
    user.roles = this.selectedOptions ;
    console.log(user);

    this.apiUserService.edit(user.id, user)
      .pipe(first())
      .subscribe(
        data => {
            alert('User updated successfully.');
            this.router.navigate(['admin/list-user']);

        },
        error => {
          alert(error);
        });
  }

  get selectedOptions() {
    return this.options
      .filter(opt => opt.checked)
      .map(opt => opt.value)
  }


}
