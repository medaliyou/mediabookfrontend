import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TokenAuthGuardService as AuthGuard} from 'ng-token-auth-symfony';
import {RouterModule, Routes} from "@angular/router";
import {IndexComponent} from "./_index/index/index.component";
import {MemberComponent} from "./_member/member/member.component";
import {RegisterComponent} from "./_register/register/register.component";
import {LoginComponent} from "./_login/login/login.component";
import {ListUserComponent} from "./admin/user/list-user/list-user.component";
import {AddUserComponent} from "./admin/user/add-user/add-user.component";
import {EditUserComponent} from "./admin/user/edit-user/edit-user.component";
import {ListBookComponent} from "./admin/book/list-book/list-book.component";
import {EditBookComponent} from "./admin/book/edit-book/edit-book.component";
import {AddBookComponent} from "./admin/book/add-book/add-book.component";
import {ListAuthorComponent} from "./admin/author/list-author/list-author.component";
import {AddAuthorComponent} from "./admin/author/add-author/add-author.component";
import {EditAuthorComponent} from "./admin/author/edit-author/edit-author.component";
import {ListEditorComponent} from "./admin/editor/list-editor/list-editor.component";
import {AddEditorComponent} from "./admin/editor/add-editor/add-editor.component";
import {EditEditorComponent} from "./admin/editor/edit-editor/edit-editor.component";
import {EditCategoryComponent} from "./admin/category/edit-category/edit-category.component";
import {AddCategoryComponent} from "./admin/category/add-category/add-category.component";
import {ListCategoryComponent} from "./admin/category/list-category/list-category.component";
import {BorrowComponent} from "./_borrow/borrow/borrow.component";


const routes: Routes = [
  {path: '', redirectTo: 'index', pathMatch: 'full'},
  { path : 'index', component : IndexComponent },

  {path: 'admin',
    children: [
      {path: 'list-user', component: ListUserComponent},
      {path: 'add-user', component: AddUserComponent},
      {path: 'edit-user', component: EditUserComponent},
      {path: 'list-book', component: ListBookComponent},
      {path: 'add-book', component: AddBookComponent},
      {path: 'edit-book', component: EditBookComponent},
      {path: 'list-author', component: ListAuthorComponent},
      {path: 'add-author', component: AddAuthorComponent, canActivate: [AuthGuard]},
      {path: 'edit-author', component: EditAuthorComponent},
      {path: 'list-editor', component: ListEditorComponent},
      {path: 'add-editor', component: AddEditorComponent, canActivate: [AuthGuard]},
      {path: 'edit-editor', component: EditEditorComponent},
      {path: 'list-category', component: ListCategoryComponent},
      {path: 'add-category', component: AddCategoryComponent, canActivate: [AuthGuard]},
      {path: 'edit-category', component: EditCategoryComponent},
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'member', component: MemberComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'borrow', component: BorrowComponent, canActivate: [AuthGuard]},

]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
