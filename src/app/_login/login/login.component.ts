import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ITokenData, TokenAuthService} from "ng-token-auth-symfony";
import {FormControl, FormGroup, NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  invalidLogin: boolean = false;



  constructor(private auth: TokenAuthService,  private router: Router) {
    if (auth.connected) {
      this.router.navigate([this.auth.config.path.login]);

    }
  }

  ngOnInit(): void {
  }

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

  submit() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
    }
  }

  public login() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
      this.auth.login(this.form.value.username, this.form.value.password).subscribe((token: ITokenData) => {
        console.log(token);


      }, (error) => {
        console.log(error);
        this.invalidLogin = true;
      } );
    }
  }

}
